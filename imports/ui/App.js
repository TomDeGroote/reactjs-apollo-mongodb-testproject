// Omdat het een react component is moet React worden geïmport, ookal wordt die niet expliciet gebruikt.
import React from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";
import ResolutionForm from "./ResolutionForm";
import GoalForm from "./GoalForm";
import { withApollo } from "react-apollo";
import Goal from "./resolutions/Goal";
import UserForm from "./UserForm";

// definitie van de app, deze krijgt een hoop ApolloProvider props binnen die we rechtstreeks ontbinden naar de nodige variabele
// loading, client zitten standaard in ApolloProvider
// resolutions en user komt erin doordat we onderaan dit bestand die queryen, zie daar voor uitleg
const App = ({ loading, resolutions, client, user }) => {

  // Kijk of de data nog aan het laden is en zo ja, return nog null
  if (loading) {
    console.log("data loading, returning null");
    return null;
  }
  console.log("data not loading anymore!");
  // Eens de data geladen is kunnen we de nodige forms teruggeven, afhankelijk van of de gebruiker is ingelogd of niet
  return (
    <div>
      {/* Userform wordt altijd getoond, maar afhankelijk van de inlog staat wordt logout, login of register getoond -> zie UserForm.js */}
      <UserForm user={user} client={client} />

      {/* als user._id naar truthy wordt geëvalueerd wordt ResolutionForm getoond (welke of null is => falsy of een waarde heeft => truthy) */}
      {user._id && <ResolutionForm />} 
      {/* Toon de resolutions */}
      {user._id && (
        <ul>
          {resolutions.map(resolution => (
            <li key={resolution._id}>
              <span
                style={{
                  textDecoration: resolution.completed ? "line-through" : "none"
                }}
              >
                {resolution.name}
              </span>
              <ul>
                {resolution.goals.map(goal => (
                  <Goal goal={goal} key={goal._id} />
                ))}
              </ul>
              <GoalForm resolutionId={resolution._id} />
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

// query die we zullen uitvoeren om de resoltions en de user op te vragen uit de backend
// dit met enkel de velden waar wij in geïnteresseerd zijn!
const resolutionQuery = gql`
  query Resolutions {
    resolutions {
      _id
      name
      completed
      goals {
        _id
        name
        completed
      }
    }
    user {
      _id
    }
  }
`;

// graphql()() neemt in de eerste () de query en argumenten, zo gebruiken we hier de resolutionQuery van hierboven
// en vullen we de data in props (aangwezige data komt reeds door de Apollo shizzle) aan met de resultaten van die query
// in de tweede () steken we onze ReactJS component met een Apollo wrapper.
export default graphql(resolutionQuery, {
  props: ({ data }) => ({ ...data })
})(withApollo(App));

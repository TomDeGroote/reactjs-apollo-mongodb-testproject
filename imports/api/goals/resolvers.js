import Goals from "./goals.js";

// Hoe moeten de mutatie operaties gedefinieerd in Goal.graphql worden verwerkt. De feitelijke logica dus.
export default {
  Mutation: {
    createGoal(obj, { name, resolutionId }, { user }) {
      if (user && user._id) {
        const goalId = Goals.insert({
          name,
          resolutionId,
          completed: false
        });
        return Goals.findOne(goalId);
      }
      throw new Error("unauthorized");
    },
    toggleGoal(obj, { _id }) {
      const goal = Goals.findOne(_id);
      Goals.update(_id, {
        $set: {
          completed: !goal.completed
        }
      });
      return Goals.findOne(goal._id);
    }
  }
};

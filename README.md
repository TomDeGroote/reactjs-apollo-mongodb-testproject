# Full stack Todo-list example - LevelUpTuts

## Stack
- ReactJS: javascript library die met JSX elementen werkt ipv DOM elementen en zo makkelijker is om te programmeren maar dankzij de babel build tool leesbaar is over da web.
- Meteor: framework die met weinig configruatie toelaat full0stack apps te starten. Kunt kiezen voor ReatJS als front end (of angular, ...) en gebruikt MongoDB als databank. Daarnaast bevat de framework ook tools voor accountmanagement.
- Apollo: implementatie van GraphQL, een nieuwe en efficiënte manier om data uit een databank te requesten.

## Installatie
TODO -> op basis van LevelUpTuts tutorial

## Uitleg bij de code
### /index.html
Wanneer meteor wordt gestart, laadt deze alle bestanden in in de root. index.html is het eerste bestand dat wordt ingeladen en dit bevat de titel en de div met id="app" waar de webcomponenten van ReactJS zullen worden ingestoken.

### /client of /server
Deze folder bevat het client/server initialisatie bestand. Het enige dat dit bestand doet is de feitelijke client/server (te vinden in de imports folder) opstarten.

### /imports
Deze folder wordt niet automatisch ingeladen door meteor en is de manier om ReactJS met meteor te laten samenwerken. De folder bevat alles van hoe de server/client moeten starten, de API en de UI.

### /imports/api
Hierin zitten de verschillende elementen van de api. Elk type heeft hier zijn eigen folder gekregen. Zo bevat /imports/api/goals bijvoorbeeld het /imports/api/goals/Goal.graphql bestand dat het type Goal beschrijft alsook welke operaties (queries, mutaties, ...) beschrijft. /imports/api/goals/goals.js wordt simpelweg gebruikt om te zeggen dat goals een collecite is in de MongoDB databank en waar die juist zitten (in welke collectie). Tot slot bevat /imports/api/goals/resolvers.js de logica van hoe de operaties (gedefinieerd in .graphql bestand) moeten worden verwerkt. Comments vertrekken vanuit de goals folder. resolutions en users gebruiken enkel comments om nieuwe dingen aan te geven. Omdat users automatisch worden behandeld door meteor is er geen /imports/api/user.js bestand.

### /imports/startup
Bevat de client en server folders voor de initialisatie van beide.

### /imports/startup/client/index.js
Maakt een nieuwe ApolloClient aan (de client van deze applicatie), geeft deze de nodige links naar de backend met HttpLink en account management met MeteorAccountsLink. Tot slot gaat meteor hier effectief de div met id="app" uit index.html invullen met de ApolloApp (die bestaat uit een ReactJS App component met een ApolloProvider wrapper).

### /imports/startup/server
Hier wordt de server geladen. Meteor zoekt de server op index.js (doro de import in /server/init.js) en in dat bestand laden we simpelweg registes-api.js in. In register-api.js registreren we effectief de types en resolvers van de API en starten we een ApolloServer.

### /imports/ui
Bevat de ReactJS componenten. App.js is het root-component en wordt in de client gewrapped met een ApolloProvider. Voor meer documentatie kan je rechtstreeks naar de bestanden kijken, best te beginnen met het top van de structuur, App.js dus.



import { ApolloServer, gql } from "apollo-server-express";
import { WebApp } from "meteor/webapp";
import { getUser } from "meteor/apollo";

import merge from "lodash/merge";

// Volgorde van de imports is hier belangrijk (vooral voor de resolvers).
// Dit komt doordat Query, Mutation gewone types zijn die maar 1 keer kunnen worden gedefinieerd. Deze worden in 
// ../../api/resolutions/resolvers voor het eerst gedefinieerd en in alle andere resolvers wordt extends type dan gebruikt.
import ResolutionsSchema from "../../api/resolutions/Resolutions.graphql";
import ResolutionsResolvers from "../../api/resolutions/resolvers";
import UsersSchema from "../../api/users/User.graphql";
import UsersResolvers from "../../api/users/resolvers";
import GoalsSchema from "../../api/goals/Goal.graphql";
import GoalsResolver from "../../api/goals/resolvers";

// Hieronder bevindt zich een comment lijn die soms aangepast wordt om meteor te forceren de .graphql bestanden te refreshen. Dit gebeurd door een bug niet automatisch bij wijzigingen.
// hsss

// Definitie van de types en resolvers
const typeDefs = [GoalsSchema, ResolutionsSchema, UsersSchema];
const resolvers = merge(GoalsResolver, ResolutionsResolvers, UsersResolvers);

// Aanmaken van de ApolloServer met de types en resolvers. Daarnaast wordt de gebruiker ook assynchroon
// in de context van de server gestoken met behulp van meteor.
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => ({
    user: await getUser(req.headers.authorization)
  })
});

// No clue...
server.applyMiddleware({
  app: WebApp.connectHandlers,
  path: "/graphql"
});

// We are doing this work-around because Playground sets headers and WebApp also sets headers
// Resulting into a conflict and a server side exception of "Headers already sent"
WebApp.connectHandlers.use("/graphql", (req, res) => {
  if (req.method === "GET") {
    res.end();
  }
});

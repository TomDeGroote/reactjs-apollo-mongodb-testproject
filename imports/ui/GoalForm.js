// React en Component impliciet nodig want het is een React Component.
import React, { Component } from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";

// mutatie query om een goal aan te maken
const createGoal = gql`
  # name en resolutionId komen binnen via de variables hieronder, mutation zegt het type van de query, createGoal geeft het een naam
  mutation createGoal($name: String!, $resolutionId: String!) {
    # name en resolutionId van hierboven, hier gebeurd de feitelijke query
    createGoal(name: $name, resolutionId: $resolutionId) {
      _id
    }
  }
`;

class GoalForm extends Component {
  // Een component bestat uit een render functie, een state en helpermethodes. Hier wordt niets in de state bijgehouden, dus afwezig

  // Helper methode die wordt opgeropen wanneer er op Submit knop wordt geklikt
  // Wordt op deze manier gedefinieerd omdat in render() onClick geen parameters kunnen halen uit de input
  // voert dus de createGoal query van hierboven uit. Als succesvol wordt de name op "" gezet, bij error wordt deze gelogd
  submitForm = () => {
    this.props
      .createGoal({
        variables: {
          name: this.name.value,
          resolutionId: this.props.resolutionId
        }
      })
      .then(() => {
        this.name.value = "";
      })
      .catch(error => {
        console.log(error);
      });
  };

  // Toont het submitform
  render() {
    return (
      <div>
        <input type="text" ref={input => (this.name = input)} />
        <button onClick={this.submitForm}>Submit</button>
      </div>
    );
  }
}

// Zoals in App.js bestaat uit graphql(query, parameters)(class), deze keer zonder Apollo wrapper want dat moet enkel in de root.
// We geven hier ook mee dat na het aanmaken van een Goal, de Resolutions query opnieuw moet worden uitgevoerd
export default graphql(createGoal, {
  name: "createGoal",
  options: {
    refetchQueries: ["Resolutions"]
  }
})(GoalForm);

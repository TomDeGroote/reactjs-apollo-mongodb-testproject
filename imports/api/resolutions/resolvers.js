import Resolutions from "./resolutions.js";
import Goals from "../goals/goals";

export default {
  Query: {
    resolutions(obj, args, { user }) {
      const userId = user ? user._id : undefined;
      console.log(userId);
      return Resolutions.find({
        userId
      }).fetch();
    }
  },

  // Een resolution heeft de velden _id, name, goals en completed. In de databank zitten echter de goals niet op een resolution en ook niet
  // of de resolution completed is. Om deze velden in te vullen worden queries uitgevoerd op de goals met de gegeven resolution._id .
  Resolution: {
    goals: resolution =>
      Goals.find({
        resolutionId: resolution._id
      }).fetch(),
    completed: resolution => {
      const goals = Goals.find({
        resolutionId: resolution._id,
      }).fetch();
      if (goals.length == 0) return false;
      const completedGoals = goals.filter(goal => goal.completed);
      return goals.length == completedGoals.length;
    }
  },

  Mutation: {
    createResolution(obj, { name }, { user }) {
      if (user) {
        const userId = user ? user._id : undefined;
        const resolutionId = Resolutions.insert({
          name,
          userId
        });
        return Resolutions.findOne(resolutionId);
      }
      throw new Error("unauthorized");
    }
  }
};

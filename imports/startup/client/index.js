import React from "react";
import { Meteor } from "meteor/meteor";
import { render } from "react-dom";
import { ApolloProvider } from "react-apollo";
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { MeteorAccountsLink } from 'meteor/apollo';
import App from "../../ui/App";

// Apollo client aanmaken. De MeteorAccountsLink zorgt ervoor dat de ApolloClient weet waar de account management gebeurd en de
// HttpLink verteld de ApolloClient waar het API-endpoint zich bevindt. Tot slot wordt er gebruik gemaakt van caching om alles
// effeciënter te maken.
const client = new ApolloClient({
  link: ApolloLink.from([
    new MeteorAccountsLink(), 
    new HttpLink({ uri: Meteor.absoluteUrl('graphql') })
  ]),
  cache: new InMemoryCache()
});

// Hier wordt de feitelijke app gemaakt wat een ReactJS root component (App) is met daarrond een ApolloProvider wrapper
// die de client bevat.
const ApolloApp = () => (
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
);

// Het feitelijke opstarten van de client, Meteor steekt hier de ApolloApp in de div met id="app" uit index.html
Meteor.startup(() => {
  render(<ApolloApp />, document.getElementById("app"));
});

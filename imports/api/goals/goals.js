import { Mongo } from 'meteor/mongo';

// Waar in de database zitten de goals?
const Goals = new Mongo.Collection("goals");

export default Goals;

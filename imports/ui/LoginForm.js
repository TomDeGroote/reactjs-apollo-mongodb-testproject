import React, { Component } from "react";

export default class LoginForm extends Component {
  // Log in met behulp van Meteor
  registerUser = event => {
    event.preventDefault();
    Meteor.loginWithPassword(this.email.value, this.password.value, error => {
      if (!error) {
        this.props.client.resetStore();
      }
      console.log(error);
    });
  };

  render() {
    return (
      <form onSubmit={this.registerUser}>
        <input type="email" ref={input => (this.email = input)} />
        <input type="password" ref={input => (this.password = input)} />
        <button type="submit">Login User</button>
      </form>
    );
  }
}
